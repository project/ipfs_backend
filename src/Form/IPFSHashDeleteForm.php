<?php

namespace Drupal\ipfs_backend\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting IPFSHash entities.
 *
 * @ingroup ipfs_backend
 */
class IPFSHashDeleteForm extends ContentEntityDeleteForm {


}
