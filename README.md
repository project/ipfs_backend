CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

IPFS -- A module for the stores the hash value of the node being inserted or
updated. This module created an custom entity which will store the node id and
hash value for the nodes. It also create a menu link "ipfs" which will show the
nodes and their hash value with the value retrieved from hash value from IPFS
server.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ipfs_backend
   https://www.ipfs.io

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/ipfs_backend


INSTALLATION
------------

 * Install the Commerce Order PDF module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.



 * Make sure IPFS Client is running at your server.
   Refer https://ipfs.io/docs/install/.

 * Run the following commands after succesfully installing the IPFS Client.
   1. ipfs init
   2. ipfs daemon

Replace the following lines from add function in
vendor/cloutier/php-ipfs-api/src/IPFS.php :-
	$req = json_decode($req, TRUE);
	return $req['Hash'];
with :-
	return $req;

CONFIGURATION
-------------

 * After installing module , Just create or update any node.

 * View list of hash values: admin > structure > IPFS Hash list.


MAINTAINERS
-----------

 * Anmol Goel - https://drupal.org/u/anmolgoyal74
 * Gaurav Kapoor - https://www.drupal.org/u/gauravkapoor

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
